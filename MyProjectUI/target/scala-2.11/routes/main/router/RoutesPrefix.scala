
// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/PROGRAMADOR 1/Desktop/MyProjectUi/conf/routes
// @DATE:Thu Jun 01 14:58:21 COT 2017


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
